pragma solidity ^0.4.25;

contract datatypes {
    string name;
    uint age = 10;

    address myaddress;
    bool mybool;
    uint8[] myintarr;

    struct account {
        uint balance;
        uint dailylimit;
    }

    account myacc;

    function simple() public {
        myacc.balance = 100;
    }
}
