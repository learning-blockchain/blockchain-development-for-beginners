pragma solidity ^0.4.25;

contract Testing {
    string name;
    uint age;

    function getname() public view returns(string) {
        return name;
    }

    function getage() public view returns(uint) {
        return age;
    }

    function setname(string argName) public {
        name = argName;
    }

    function setage(uint argAge) public {
        age = argAge;
    }
}