pragma solidity ^0.4.25;
pragma experimental ABIEncoderV2;

contract mappings {
    string s;
    string[] arrstr;

    function stt(string[] b) ownerOnly public view returns(string[]) {
        return b;
    }

    address private owner;

    modifier ownerOnly() {
        require(owner == msg.sender);
        _;
    }

    struct Acc {
        uint bal;
        uint DailyLimit;
    }

    Acc account;

    mapping(address => Acc) TheAccounts;
}
