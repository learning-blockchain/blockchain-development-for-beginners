pragma solidity ^0.4.25;

contract inlining {
    function looping() public pure returns(uint r) {
        for (uint i = 0; i < 10; i++) {
            r++;
        }
    }

    function loopinginline() public returns(uint r) {
        assembly {
            let i := 0

            loop:
            i := add(i, 1)
            r := add(r, 1)
            jumpi(loop, lt(i, 10))
        }
    }

    function condi(uint v) public pure returns(uint) {
        if (v == 5) {
            return 55;
        }
        else if (v == 6) {
            return 66;
        }
        return 111;
    }

    function condition(uint v) public pure returns(uint) {
        assembly {
            switch v
            case 5 {
                r := 55
            }
            case 6 {
                r := 66
            }
            default {
                r := 111
            }
        }
    }
}
